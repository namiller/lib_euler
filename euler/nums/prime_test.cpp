#include "euler/nums/primes.h"
#include "gtest/gtest.h"
#include <vector>
#include <map>
#include <set>
#include <stdlib.h>

using namespace std;

TEST(PrimesToN, size) {
  EXPECT_EQ(4, primes_to_n(10).size());
  EXPECT_EQ(0, primes_to_n(1).size());
  EXPECT_EQ(1, primes_to_n(2).size());
  EXPECT_EQ(1000, primes_to_n(7919).size());
  EXPECT_EQ(1000, primes_to_n(7920).size());
}

TEST(PrimesToN, primality) {
  for (const auto & p : primes_to_n(1000)) {
    EXPECT_EQ(true, is_prime(p));
  }
}

TEST(NPrimes, size) {
  for (int i = 0; i < 500; i++) {
    EXPECT_EQ(i, n_primes(i).size());
  }
}

TEST(NPrimes, composition) {
  for (int i = 500; i < 1000; i+=100) {
    vector<int> l1 = primes_to_n(i);
    EXPECT_EQ(l1, n_primes(l1.size()));
  }
}

TEST(IsPrime, ten_digit_primes) {
  EXPECT_EQ(true, is_prime(5915587277LL));
  EXPECT_EQ(true, is_prime(1500450271LL));
  EXPECT_EQ(true, is_prime(3267000013LL));
  EXPECT_EQ(true, is_prime(5754853343LL));
  EXPECT_EQ(true, is_prime(4093082899LL));
  EXPECT_EQ(true, is_prime(9576890767LL));
  EXPECT_EQ(true, is_prime(3628273133LL));
  EXPECT_EQ(true, is_prime(2860486313LL));
  EXPECT_EQ(true, is_prime(5463458053LL));
  EXPECT_EQ(true, is_prime(3367900313LL));
}

TEST(IsPrime, almost_prime) {
  EXPECT_EQ(false, is_prime(21727LL*23767LL));
  EXPECT_EQ(false, is_prime(33713LL*83873LL));
  EXPECT_EQ(false, is_prime(34591LL*84731LL));
  EXPECT_EQ(false, is_prime(70001LL*4931LL));
  EXPECT_EQ(false, is_prime(85819LL*22229LL));
}
TEST(IsPrime, squares) {
  for (long long i = 0; i < 1000; i++) {
    EXPECT_EQ(false, is_prime(i*i));
  }
}

TEST(IsPrime, edge) {
  EXPECT_EQ(false, is_prime(1));
  EXPECT_EQ(false, is_prime(0));
  EXPECT_EQ(false, is_prime(-1));
  EXPECT_EQ(false, is_prime(-2));
  EXPECT_EQ(true, is_prime(2));
}

TEST(UniquePrimeFactors, simple) {
  EXPECT_EQ(2, unique_prime_factors(12).size());
}

TEST(UniquePrimeFactors, primes) {
  for (const auto & p : n_primes(100)) {
    vector<int> factors = unique_prime_factors(p);
    EXPECT_EQ(1, factors.size());
    EXPECT_EQ(p, factors[0]);
  }
}

TEST(UniquePrimeFactors, squares) {
  for (int i = 1; i < 100; i++) {
    EXPECT_EQ(unique_prime_factors(i), unique_prime_factors(i*i));
  }
}

TEST(UniquePrimeFactors, repeated) {
  EXPECT_EQ(vector<int>({2,3}), unique_prime_factors(2*2*3));
  EXPECT_EQ(vector<int>({2,5}), unique_prime_factors(1000));
  EXPECT_EQ(vector<int>({2,3,5,7}), unique_prime_factors(2*2*2*3*5*7*7*7));
  EXPECT_EQ(vector<int>({5,7,11}), unique_prime_factors(5*7*11*11));
}

TEST(UniquePrimeFactors, non_repeated) {
  EXPECT_EQ(vector<int>({2,3}), unique_prime_factors(2*3));
  EXPECT_EQ(vector<int>({2,5}), unique_prime_factors(10));
  EXPECT_EQ(vector<int>({2,3,5,7}), unique_prime_factors(2*3*5*7));
  EXPECT_EQ(vector<int>({5,7,11}), unique_prime_factors(5*7*11));
}

TEST(UniquePrimeFactors, uniqueness) {
  for (int i = 1; i < 10000; i++) {
    vector<int> factors = unique_prime_factors(i);
    set<int> s;
    for (const auto &f : factors) {
      s.insert(f);
    }
    EXPECT_EQ(s.size(), factors.size());
  }
}

TEST(PrimeFactors, recomposition) {
  for (int i = 1; i < 100000; i++) {
    auto fs = prime_factors(i);
    int prod = 1;
    for (const auto &f : fs) {
      prod *= f;
    }
    EXPECT_EQ(prod, i);
  }
}

TEST(PrimeFactors, squares) {
  for (int i = 1; i < 1000; i++) {
    EXPECT_EQ(prime_factors(i).size() * 2, prime_factors(i*i).size());
  }
}

TEST(PrimeFactors, repeated) {
  EXPECT_EQ(vector<int>({2,2,3}), prime_factors(2*2*3));
  EXPECT_EQ(vector<int>({2,2,2,5,5,5}), prime_factors(1000));
  EXPECT_EQ(vector<int>({2,2,2,3,5,7,7,7}), prime_factors(2*2*2*3*5*7*7*7));
  EXPECT_EQ(vector<int>({5,7,11,11}), prime_factors(5*7*11*11));
}

TEST(PrimeFactors, non_repeated) {
  EXPECT_EQ(vector<int>({2,3}), prime_factors(2*3));
  EXPECT_EQ(vector<int>({2,5}), prime_factors(10));
  EXPECT_EQ(vector<int>({2,3,5,7}), prime_factors(2*3*5*7));
  EXPECT_EQ(vector<int>({5,7,11}), prime_factors(5*7*11));
}

TEST(ProbablyPrime, consistancy) {
  for (int i = 1; i < 100000; i++) {
    EXPECT_EQ(probably_prime(i), is_prime(i));
  }
}

TEST(ProbablyPrime, fuzz) {
  srand(123);
  for (int i = 0; i < 1000; i++) {
    auto r = rand();
    EXPECT_EQ(probably_prime(r), is_prime(r));
  }
}

TEST(ProbablyPrime, primes) {
  for (const auto& prime : n_primes(100)) {
		EXPECT_TRUE(probably_prime(prime));
	}
}

TEST(ProbablyPrime, pseudo) {
  EXPECT_EQ(false, probably_prime(90751)); // 2
  EXPECT_EQ(false, probably_prime(97567)); // 3
  EXPECT_EQ(false, probably_prime(79381)); // 5
  EXPECT_EQ(false, probably_prime(25326001)); // 2, 3, 5
  EXPECT_EQ(false, probably_prime(3825123056546413051LL)); // 2,3,5,7,11,13,17,19,23
}

TEST(PrimePowers, large) {
  EXPECT_EQ((map<int, int>({{2,2},{3,1}})), prime_factor_powers(2*2*3));
  EXPECT_EQ((map<int, int>({{2,3},{5,3}})), prime_factor_powers(1000));
  EXPECT_EQ((map<int, int>({{2,3},{3,1},{5,1},{7,3}})), prime_factor_powers(2*2*2*3*5*7*7*7));
  EXPECT_EQ((map<int, int>({{5,1},{7,1},{11,2}})), prime_factor_powers(5*7*11*11));
}

TEST(PrimePowers, small_reconstructed) {
	for (int i = 1; i < 10000; i++) {
		auto m = prime_factor_powers(i);
		int r = 1;
		for (const auto p : m) {
			for (int x = 0; x < p.second; x++) {
				r *= p.first;
			}
		}
		EXPECT_EQ(i, r);
	}
}


