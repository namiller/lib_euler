
// A handy factor caching number that supports efficient operations on
// a factored or unfactored representation.
template<typename Num>
class LazyNumber<Num> {
 public:
	LazyNumber(Num v);
	LazyNumber(map<Num, Num> factors);

	// Forces the number to factorize.
	void force_factor();
	// Forces the number to evaluate.
	void force_eval();

	

 private:	
	bool has_factors;
	bool has_value;
};
